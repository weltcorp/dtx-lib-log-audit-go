## Usage

```go
type AMQPConfig struct {
    Server   		string
    Username 		string
    Password     	string
    Exchange     	string
    Port			string
}
cfg := AMQPConfig{
	Server = "",
	Username = "",
	Password = "",
	Exchange = "",
	Port = "",
}
Init(cfg, os_env)
```

```go
func Middleware() *grpc.Server {
	server := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			...
			auditLog.AuditServerUnaryInterceptor()
		)),
	)
	return server
}
```