package auditLog

import (
	statusPb "bitbucket.org/weltcorp/dtx-lib-grpc-go/proto"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	systemLog "log"
	"net/http"
	"path"
	"strconv"
	"strings"
	"time"

	dtxBootstrap "bitbucket.org/weltcorp/dtx-lib-bootstrap-go"
	dtxBootstrapLog "bitbucket.org/weltcorp/dtx-lib-bootstrap-go/log"
	dtxError "bitbucket.org/weltcorp/dtx-lib-grpc-go/error"
	"bitbucket.org/weltcorp/dtx-lib-iam-go/ctxauth"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	amqp "github.com/rabbitmq/amqp091-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type AMQPConfig struct {
	Server   string
	Username string
	Password string
	Port     string
}

type DTxConfig struct {
	DomainId    int
	ServiceName string
}

type Audit struct {
	LogType        string  `json:"logType"`
	Message        message `json:"message"`
	DomainId       int     `json:"domainId"`
	SrcDomainId    int     `json:"srcDomainId"`
	ServiceName    string  `json:"serviceName"`
	SrcServiceName string  `json:"srcServiceName"`
	ProjectId      int     `json:"projectId"`
	GroupId        int     `json:"groupId"`
	SrcProjectId   int     `json:"srcProjectId"`
	SrcPlatform    string  `json:"srcPlatform"`
	UserId         int     `json:"userId"`
	UserType       int     `json:"userType"`
	ObjectTypeId   int     `json:"objectTypeId"`
	Action         string  `json:"action"`
	OsEnv          string  `json:"osEnv"`
}

type message struct {
	Request  request  `json:"request"`
	Response response `json:"response"`
}

type request struct {
	Method    string `json:"method"`
	Url       string `json:"url"`
	UrlRoute  string `json:"url_route"`
	Query     string `json:"query"`
	Headers   string `json:"headers"`
	Timestamp string `json:"timestamp"`
	Ip        string `json:"ip"`
	Body      string `json:"body"`
}

type response struct {
	StatusCode int    `json:"status_code"`
	Timestamp  string `json:"timestamp"`
	Elapsed    int    `json:"elapsed"`
	Body       string `json:"body"`
}

type Client struct {
	ch         *amqp.Channel
	conn       *amqp.Connection
	connOpened chan bool
	chOpened   chan bool
	running    bool
}

var c *Client

var MQ_EXCHANGE_AUDIT_LOG = "audit.log.send.direct"
var osEnv string
var dtxConfig DTxConfig
var amqpConfig AMQPConfig
var TLScfg bool
var libName = "dtx-lib-log-audit-go"

func Init(cfg interface{}, TLS bool) (*Client, error) {
	var err error
	TLScfg = TLS

	amqpConfig, err = getConfig(cfg)
	if err != nil {
		return nil, err
	}

	c = &Client{connOpened: make(chan bool), chOpened: make(chan bool), running: false}
	openConnectionWithRetry(amqpConfig, TLScfg)

	err = c.openChannel()
	if err != nil {
		return nil, fmt.Errorf("[%s] Channel Error: %w", libName, err)
	}
	c.running = true
	return c, nil
}

func openConnection(amqpConfig AMQPConfig, TLS bool) error {
	var err error
	var conn *amqp.Connection
	if TLS {
		tlsCfg := new(tls.Config)
		conn, err = amqp.DialTLS("amqps://"+amqpConfig.Username+":"+amqpConfig.Password+"@"+
			amqpConfig.Server+":"+amqpConfig.Port, tlsCfg)
	} else {
		conn, err = amqp.Dial("amqp://" + amqpConfig.Username + ":" + amqpConfig.Password + "@" +
			amqpConfig.Server + ":" + amqpConfig.Port)
	}
	if err != nil {
		return fmt.Errorf("amqp.Dial() error: %w", err)
	}
	c.conn = conn
	go c.notifyConnectionClose()
	systemLog.Printf("[%s] AMQP Connection Opened", libName)
	return nil
}

func (c *Client) notifyConnectionClose() {
	err := <-c.conn.NotifyClose(make(chan *amqp.Error))
	systemLog.Printf("[%s] AMQP Connection Error: NotifyClose received", libName)
	if err != nil {
		openConnectionWithRetry(amqpConfig, TLScfg)
		go func() {
			c.connOpened <- true
		}()
	}
}

func (c *Client) openChannel() error {
	var err error
	c.ch, err = c.conn.Channel()
	if err != nil {
		return fmt.Errorf("openChannel() error: %w", err)
	}
	go func() {
		c.chOpened <- true
	}()
	c.running = true
	go c.notifyChannelClose()
	return nil
}

func (c *Client) notifyChannelClose() {
	err := <-c.ch.NotifyClose(make(chan *amqp.Error))
	if err != nil {
		c.running = false
		c.chOpened = make(chan bool)
		if c.conn != nil || c.conn.IsClosed() {
			<-c.connOpened
		}
		c.openChannelWithRetry()
		c.running = true
	}
}

func openConnectionWithRetry(amqpConfig AMQPConfig, TLS bool) {
	var err error
	n := 0
	for n < 30 {
		if n > 0 {
			time.Sleep(2 * time.Second)
		}
		err = openConnection(amqpConfig, TLS)
		if err != nil {
			if n > 0 && n%5 == 0 {
				dtxBootstrapLog.E(fmt.Sprintf("[%s] Cannot connect to AMQP: %s", libName, err.Error()))
			}
			n += 1
		} else {
			break
		}
	}
	if err != nil {
		dtxBootstrapLog.E(err.Error())
		panic(err)
	}
}

func (c *Client) openChannelWithRetry() {
	var err error
	n := 0
	for n < 15 {
		if n > 0 {
			time.Sleep(2 * time.Second)
		}
		err = c.openChannel()
		if err != nil {
			n += 1
		} else {
			break
		}
	}
	if err != nil {
		dtxBootstrapLog.E(err.Error())
		systemLog.Printf("[%s] MQ Channel Failed", libName)
		panic(err)
	}
}

func (c *Client) Shutdown() error {
	if err := c.ch.Cancel("", true); err != nil {
		return fmt.Errorf("[%s] channel cancel failed: %w", libName, err)
	}
	if err := c.conn.Close(); err != nil {
		return fmt.Errorf("[%s] AMQP connection close error: %w", libName, err)
	}
	return nil
}

func getConfig(cfg interface{}) (AMQPConfig, error) {
	var field []string
	field = append(field, "MQ_AUDIT_HOST", "MQ_AUDIT_PASSWORD", "MQ_AUDIT_PORT", "MQ_AUDIT_USER",
		"DOMAIN_ID", "SERVICE_NAME", "ENV")
	res, err := dtxBootstrap.GetFieldFromStruct(cfg, field)
	if err != nil {
		return AMQPConfig{}, fmt.Errorf("[%s] getConfig error: %w", libName, err)
	}
	osEnv = res["ENV"]

	domainId, _ := strconv.Atoi(res["DOMAIN_ID"])
	dtxConfig.DomainId = domainId
	dtxConfig.ServiceName = res["SERVICE_NAME"]
	return AMQPConfig{
		Server:   res["MQ_AUDIT_HOST"],
		Username: res["MQ_AUDIT_USER"],
		Password: res["MQ_AUDIT_PASSWORD"],
		Port:     res["MQ_AUDIT_PORT"],
	}, nil
}

func log(message Audit) error {
	if !c.running {
		<-c.chOpened
		go func() {
			c.chOpened <- true
		}()
	}
	m, err := json.Marshal(message)
	if err != nil {
		return fmt.Errorf("json.Marshal() error: %w", err)
	}
	err = c.ch.Publish(
		MQ_EXCHANGE_AUDIT_LOG,
		"",    // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        m,
		})

	if err != nil {
		systemLog.Printf("[%s] Publish Error: %s", libName, err.Error())
		return fmt.Errorf("[%s] Publish Error: %w", libName, err)
	}
	return nil
}

type ErrorToCode func(err error) codes.Code

type DurationToField func(duration time.Duration) (key string, value interface{})

func FuncErrorToCode(err error) codes.Code {
	return status.Code(err)
}

var DefaultErrorToCode ErrorToCode = FuncErrorToCode

// DefaultDurationToField is the default implementation of converting request duration to a log field (key and value).
var DefaultDurationToField DurationToField = DurationToTimeMillisField

// DurationToTimeMillisField converts the duration to milliseconds and uses the key `grpc.time_ms`.
func DurationToTimeMillisField(duration time.Duration) (key string, value interface{}) {
	return "grpc.time_ms", durationToMilliseconds(duration)
}

// DurationToDurationField uses the duration value to log the request duration.
func DurationToDurationField(duration time.Duration) (key string, value interface{}) {
	return "grpc.duration", duration
}

func durationToMilliseconds(duration time.Duration) float32 {
	return float32(duration.Nanoseconds()/1000) / 1000
}

func marshalApiError(err error) (int, error) {
	s := status.Convert(err)
	pb := s.Proto()
	if pb.GetDetails() != nil {
		anyPb := pb.GetDetails()[0]
		res := &statusPb.ErrorStatus{}

		pErr := anyPb.UnmarshalTo(res)
		if pErr != nil {
			return 0, pErr
		}
		return int(res.Status), nil
	}
	return 0, nil
}

func AuditServerUnaryInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		startTime := time.Now()
		startMs := time.Now().UnixMilli()
		service := path.Dir(info.FullMethod)[1:]
		method := path.Base(info.FullMethod)

		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Errorf(codes.DataLoss, "failed to get metadata")
		}
		_auth := ctxauth.Extract(ctx)
		auth := _auth.Values()

		prevError := false
		if _auth.Has("auth.error") {
			prevError = auth["auth.error"].(bool)
		}

		var resp interface{}
		var handlerError error
		if prevError {
			resp = nil
			errorGrpcCode := codes.Unknown
			if _auth.Has("auth.errorGrpcCode") {
				errorGrpcCode = auth["auth.errorGrpcCode"].(codes.Code)
			}
			errorStatus := http.StatusBadRequest
			if _auth.Has("auth.errorStatus") {
				errorStatus = int(auth["auth.errorStatus"].(uint32))
			}
			errorCode := dtxError.UNKNOWN_ERROR
			if _auth.Has("auth.errorCode") {
				errorCode = int(auth["auth.errorCode"].(uint32))
			}
			errorMessage := "Unknown"
			if _auth.Has("auth.errorMessage") {
				errorMessage = auth["auth.errorMessage"].(string)
			}
			handlerError = dtxError.ApiError(
				errorGrpcCode,
				int32(errorStatus),
				int32(errorCode),
				errorMessage,
				nil)
		} else {
			resp, handlerError = handler(ctx, req)
		}

		c := DefaultErrorToCode(handlerError)
		code := runtime.HTTPStatusFromCode(c)

		if handlerError != nil {
			apiErrCode, err := marshalApiError(handlerError)
			if err != nil {
				systemLog.Printf("AuditServerUnaryInterceptor/marshalApiError error : %s", err.Error())
			}
			if apiErrCode > 0 {
				code = apiErrCode
			}
		}

		stealth := md.Get("x-request-dtx-stealth")
		if len(stealth) > 0 && stealth[0] == "true" {
			return resp, handlerError
		}

		clientIp := ""
		xForwardFor := md.Get("x-forwarded-for")
		if len(xForwardFor) > 0 && xForwardFor[0] != "" {
			ips := strings.Split(xForwardFor[0], ",")
			if len(ips) > 0 {
				clientIp = ips[0]
			}
		}

		endMs := time.Now().UnixMilli()

		_, durVal := DefaultDurationToField(time.Since(startTime))
		str := fmt.Sprintf("%v", durVal)
		e, _ := strconv.ParseFloat(str, 64)
		elapsed := int(e)

		header, err := json.Marshal(md)
		var auditLog Audit

		var srcDomainId = 100
		if _auth.Has("auth.domainId") {
			srcDomainId = int(auth["auth.domainId"].(int64))
		}
		var projectId = 0
		if _auth.Has("auth.projectId") {
			projectId = int(auth["auth.projectId"].(int64))
		}
		var groupId = 0
		if _auth.Has("auth.groupId") {
			groupId = int(auth["auth.groupId"].(int64))
		}
		var accountId = 2
		if _auth.Has("auth.accountId") {
			accountId = int(auth["auth.accountId"].(int64))
		}
		var accountType = 1
		if _auth.Has("auth.accountType") {
			accountType = int(auth["auth.accountType"].(int64))
		}
		var srcServiceName = "unknown"
		if _auth.Has("auth.serviceName") {
			srcServiceName = auth["auth.serviceName"].(string)
		}
		var srcPlatform = "unknown"
		if _auth.Has("auth.platform") {
			srcPlatform = auth["auth.platform"].(string)
		}

		auditLog.LogType = "api"
		auditLog.DomainId = dtxConfig.DomainId
		auditLog.SrcDomainId = srcDomainId
		auditLog.ServiceName = dtxConfig.ServiceName
		auditLog.SrcServiceName = srcServiceName
		auditLog.ProjectId = projectId
		auditLog.GroupId = groupId
		auditLog.SrcPlatform = srcPlatform
		auditLog.UserId = accountId
		auditLog.UserType = accountType
		auditLog.ObjectTypeId = 0
		auditLog.Action = "request"
		auditLog.OsEnv = osEnv

		auditLog.Message.Request.UrlRoute = method
		auditLog.Message.Request.Method = service
		auditLog.Message.Request.Timestamp = strconv.FormatInt(startMs, 10)
		auditLog.Message.Request.Ip = clientIp
		auditLog.Message.Request.Headers = string(header)
		auditLog.Message.Request.Body = JSONFormatter(fmt.Sprintf("%v", req))

		auditLog.Message.Response.Elapsed = elapsed
		auditLog.Message.Response.StatusCode = code
		auditLog.Message.Response.Timestamp = strconv.FormatInt(endMs, 10)
		auditLog.Message.Response.Body = JSONFormatter(fmt.Sprintf("%v", resp))

		go func() {
			if err = log(auditLog); err != nil {
				dtxBootstrapLog.E(err.Error())
			}
		}()

		return resp, handlerError
	}
}

func JSONFormatter(payload string) string {
	if payload = "{" + payload + "}"; payload == "{}" {
		return payload
	}
	json := strings.Replace(payload, " ", ",\"", -1)
	json = strings.Replace(json, "{", "{\"", -1)
	json = strings.Replace(json, ":", "\":", -1)
	return json
}
